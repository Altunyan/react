import {ADD_DROPPABLE, DELETE_DROPPABLE} from "../constants/action-types";

const initialState = {
    droppable: []
};

function rootReducer(state = initialState, action) {
    if (action.type === ADD_DROPPABLE) {
        return Object.assign({}, state, {
            droppable: state.droppable.concat(action.payload)
        });
    }

    if (action.type === DELETE_DROPPABLE) {
        return Object.assign({}, state, {
            droppable: state.droppable.slice(0, action.payload).concat(state.droppable.slice(action.payload + 1, state.droppable.length))
        });
    }

    return state;
}

export default rootReducer;
