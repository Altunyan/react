import React from 'react'
import ReactDOM from 'react-dom'
import './css/index.css'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import App from './App'
import Dropdown from './components/Dropdown'

const routing = (
  <Router>
    <div>
      <Route exact path="/" component={Dropdown} />
    </div>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))
