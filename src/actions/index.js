import { ADD_DROPPABLE ,DELETE_DROPPABLE} from "../constants/action-types";

export function addDroppable(payload) {
  return { type: ADD_DROPPABLE, payload };
}

export function deleteDroppable(payload) {
  return { type: DELETE_DROPPABLE, payload };
}
