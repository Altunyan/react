import * as React from "react";
import {Component} from 'react';
import Team from './Team'

class League extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        }
    }

    fold() {
        let active = (this.state.active) ? false : true
        this.setState({active: active});
    }

    render() {
        return (
            <li>
                <span className={(this.state.active) ? "caret caret-down" : "caret"}
                      onClick={this.fold.bind(this)}>{this.props.league.name}</span>
                <ul className={(this.state.active) ? "nested active" : "nested"}>
                    {this.props.league.teams.map((team, key) =>
                        <Team team={team}/>
                    )}
                </ul>
            </li>
        )
    }
}

export default League;
