import * as React from "react";
import {Component} from 'react';
import Player from './Player'
import {DragSource} from 'react-dnd';
import store from "../store/index";
import {addDroppable, deleteDroppable} from "../actions/index";

const itemSource = {
    beginDrag(props) {
        return props.team;
    },
    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            return;
        }
        let store_arr = store.getState().droppable,
            player = false,
            team = false;

        store_arr.map((element, key) => {
            if (element.type === 1) player = true;
            if (element === props.team) team = true;
        });

        if (!player && !team) {
            store.dispatch(addDroppable(props.team));
            if (store_arr.length === 2) store.dispatch(deleteDroppable(0))
        }
    }
};

class Team extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            selected: false,
            disabled: false
        };

        store.subscribe(() => {
            let store_arr = store.getState().droppable,
                team = this.props.team;

            this.setState({
                selected: false,
                disabled: false
            });

            store_arr.map((element, key) => {
                if (element === team) {
                    this.setState({selected: true})
                } else if (element.type === 1) {
                    this.setState({disabled: true})
                }
            })
        });
    }

    fold() {
        let active = (this.state.active) ? false : true
        this.setState({active: active});
    }


    static collect(connect, monitor) {
        return {
            connectDragSource: connect.dragSource(),
            connectDragPreview: connect.dragPreview(),
            isDragging: monitor.isDragging(),
        }
    }


    render() {
        const {connectDragSource, team} = this.props;

        return connectDragSource(
            <li>
                <span
                    className={[(this.state.active) ? "caret caret-down" : "caret", (this.state.selected) ? "selected" : "draggable", (this.state.disabled) ? "disabled" : ""].join(' ')}
                    onClick={this.fold.bind(this)}>{team.name}</span>
                <ul className={(this.state.active) ? "nested active" : "nested"}>
                    {this.props.team.players.map((player, key) =>
                        <Player player={player}/>
                    )}
                </ul>
            </li>
        )
    }
}

export default DragSource('draggable', itemSource, Team.collect)(Team);
