import * as React from "react";
import {Component} from 'react';
import League from './League';
import Area from './Area';
import '../css/dropdown.css';
import HTML5Backend from 'react-dnd-html5-backend';
import {DragDropContext} from 'react-dnd';


class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            data: [
                {
                    name: 'Premier League',
                    teams: [
                        {
                            type: 0,
                            name: 'Manchester United',
                            budget: '500M EUR',
                            img: "/img/manchester.png",
                            players: [
                                {
                                    type: 1,
                                    name: 'Pogba',
                                    goals: 20,
                                    appearances: 30,
                                    tackle: 10,
                                    img: "/img/pogba.png"
                                },
                                {
                                    type: 1,
                                    name: 'de Gea',
                                    goals: 1,
                                    appearances: 41,
                                    tackle: 25,
                                    img: "/img/de Gea.png"
                                },
                            ]
                        },
                        {
                            type: 0,
                            name: 'Liverpool',
                            budget: '400M EUR',
                            img: "/img/liverpool.png",
                            players: [
                                {
                                    type: 1,
                                    name: 'Mohamed Salah',
                                    goals: 30,
                                    appearances: 44,
                                    tackle: 20,
                                    img: "/img/mohamed.png"
                                },
                                {
                                    type: 1,
                                    name: 'Alisson',
                                    goals: 0,
                                    appearances: 30,
                                    tackle: 50,
                                    img: "/img/alisson.png"
                                }
                            ]
                        },
                    ]
                },
                {
                    name: 'Serie A',
                    teams: [
                        {
                            type: 0,
                            name: 'Juventus',
                            budget: '450M EUR',
                            img: "/img/juventus.png",
                            players: [
                                {
                                    type: 1,
                                    name: 'C. Ronaldo',
                                    goals: 50,
                                    appearances: 50,
                                    tackle: 23,
                                    img: "/img/ronaldo.png"
                                },
                                {
                                    type: 1,
                                    name: 'Kin',
                                    goals: 8,
                                    appearances: 20,
                                    tackle: 16,
                                    img: "/img/kin.png"
                                }
                            ]
                        },
                        {
                            type: 0,
                            name: 'AC Milan',
                            budget: '300M EUR',
                            img: "/img/milan.png",
                            players: [
                                {
                                    type: 1,
                                    name: 'Krzysztof Piątek',
                                    goals: 12,
                                    appearances: 20,
                                    tackle: 35,
                                    img: "/img/krzysztof-piatek.png"
                                },
                                {
                                    type: 1,
                                    name: 'Gianluigi Donnarumma',
                                    goals: 1,
                                    appearances: 30,
                                    tackle: 44,
                                    img: "/img/Donnaruma.png"
                                }
                            ]
                        },
                    ],
                }
            ]
        }

    }


    render() {
        return (
            <div>
                <ul id="dropdown">
                    {
                        this.state.data.map((league, key) =>
                            <League league={league}/>
                        )
                    }
                </ul>
                <Area/>
            </div>

        );
    }
}

export default DragDropContext(HTML5Backend)(Dropdown)
