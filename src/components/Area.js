import * as React from "react";
import {Component} from 'react';
import '../css/area.css';
import {DropTarget} from 'react-dnd';
import AreaCard from './AreaCard';
import store from "../store/index";
import {deleteDroppable} from "../actions/index";

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
    }
}

class Area extends Component {
    constructor(props) {
        super(props);
        this.state = {
            droppable: [],
        };
        store.subscribe(() => {
            this.setState({droppable: store.getState().droppable})
        });
    }

    deleteDroppable = (index) => {
        store.dispatch(deleteDroppable(index))
    };

    compare_objects(obj0, obj1) {
        let comp = {};
        Object.keys(obj0).map((prop, key) => {
            if (prop !== 'name' && prop !== 'type') {
                if (parseInt(obj0[prop]) > parseInt(obj1[prop])) {
                    comp[prop] = 0
                } else if (parseInt(obj0[prop]) < parseInt(obj1[prop])) {
                    comp[prop] = 1
                } else {
                    comp[prop] = ""
                }
            }

        });
        return comp;
    }

    render() {
        const {connectDropTarget} = this.props;
        let comp = {};
        if (this.state.droppable.length === 2) {
            comp = this.compare_objects(this.state.droppable[0], this.state.droppable[1])
        }
        return connectDropTarget(
            <div className="area">
                {
                    this.state.droppable.map((droppable, key) =>
                        <AreaCard droppable={droppable} comp={comp} deleteDroppable={this.deleteDroppable} index={key}/>
                    )
                }
            </div>
        )
    }
}

export default DropTarget('draggable', {}, collect)(Area);
