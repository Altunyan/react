import * as React from "react";
import {Component} from 'react';
import {DragSource} from 'react-dnd';
import store from "../store/index";
import {addDroppable, deleteDroppable} from "../actions/index";

const itemSource = {
    beginDrag(props) {
        return props.player;
    },
    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            return;
        }

        let store_arr = store.getState().droppable,
            team = false;

        store_arr.map((element, key) => {
            if (element.type === 0) team = true
        });

        if (!team) {
            store.dispatch(addDroppable(props.player))
            if (store_arr.length === 2) store.dispatch(deleteDroppable(0))
        }
    }
};

class Player extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            disabled: false
        };

        store.subscribe(() => {
            let store_arr = store.getState().droppable,
                player = this.props.player;

            this.setState({
                selected: false,
                disabled: false
            });

            store_arr.map((element, key) => {
                if (element === player) {
                    this.setState({selected: true})
                } else if (element.type === 0) {
                    this.setState({disabled: true})
                }
            })
        });

    }

    static collect(connect, monitor) {
        return {
            connectDragSource: connect.dragSource(),
            connectDragPreview: connect.dragPreview(),
            isDragging: monitor.isDragging(),
        }
    }

    render() {
        const {connectDragSource, player} = this.props;
        return connectDragSource(
            <li className={[(this.state.selected) ? "selected" : "draggable", (this.state.disabled) ? "disabled" : ""].join(' ')}>{player.name}</li>
        )
    }
}

export default DragSource('draggable', itemSource, Player.collect)(Player);
