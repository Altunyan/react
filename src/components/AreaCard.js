import * as React from "react";
import {Card, ListGroup, ListGroupItem} from 'react-bootstrap';

class AreaCard extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClick = (e) => {
        e.preventDefault();
        this.props.deleteDroppable(this.props.index);
    };

    render() {
        const {droppable, comp, index} = this.props;

        return <Card style={{width: '18rem'}}>
            <Card.Body>
                <Card.Title>{droppable.name}
                    <button type="button" className="close" aria-label="Close" onClick={this.handleClick}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Card.Title>
            </Card.Body>
            <Card.Img variant="top" src={droppable.img}/>
            <ListGroup className="list-group-flush">
                {Object.keys(droppable).map((prop, key) => {
                    if (prop !== 'name' && prop !== 'players' && prop !== 'type' && prop !== 'img') {
                        let color = "";
                        if (Object.keys(comp).length > 0) {
                            if (comp[prop] === "") {
                                color = 'yellow';
                            } else if (comp[prop] === index) {
                                color = 'green';
                            } else {
                                color = 'red';
                            }
                        }

                        return <ListGroupItem>{prop} - <span
                            className={color}>{this.props.droppable[prop]}</span></ListGroupItem>
                    }

                })}
            </ListGroup>
        </Card>;
    }
}

export default AreaCard;
